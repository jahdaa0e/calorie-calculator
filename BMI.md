# BMI Calculator

## Formula
BMI = (Weight)/(Height^2)


BMI = kg/m2 


Healthy weight = Normal BMI Range (18.5 - 25 ) * Height (m)^2


|Category          | BMI range - kg/m2  |
|--------------    | ------------------ |
|Severe Thinness   |	< 16            |
|Moderate Thinness |	16 - 17         |
|Mild Thinness	   | 17 - 18.5          |
|Normal	           | 18.5 - 25          |
|Overweight	       | 25 - 30            |
|Obese Class I	   | 30 - 35            |
|Obese Class II	   | 35 - 40            |
|Obese Class III   |  > 40              |



### Example

Height = 1.5 m
Weight = 50 kg


BMI = 50/(1.5^2)

#### Healthy weight
low: 18.5 * (1.5 ^ 2)
High: 25 * ( 1.5 ^ 2)

