//Built-in JS Functionalities: ----------------------------------------------------------------------

//Memory information
//const os = require("os");
// free memory
// console.log(os.freemem());
// total memory
// console.log(os.totalmem());

//File Path
// const path = require("path");
// const mypath = path.parse(__filename);
// console.log(mypath);

//File System ------------------------------------------------------------------------------

//File Systems: reading files from the system example
//var fs = require("fs");
// const readMe = fs.readFileSync("data.txt",'utf-8')
//console.log(readMe);

//Copying the content from a file to another:
// fs.writeFileSync("des.txt",readMe);

//Prenting the current directory files
// const files = fs.readdirSync('./');
// console.log(files);

// files.forEach((ele)=>{
//     //print all elements
//     console.log(ele);
//     //reading specific element
//     console.log(files[0]);
// })

//fs.readFile('data.txt', 'utf-8', (err, data) => {
//    console.log(data);
//});

//Copying file from one to another:

// fs.readFile('data.txt', 'utf-8', (err, data) => {
//     console.log(data);
//     fs.writeFile('des2.txt',data, (err, data) => {});
// });

//HTTP------------------------------------------------------------------------------------------------------------------------------------------

//1- Create JS Server:
// var http = require('http');
//createing:
// const server = http.createServer();
// console.log(server);
//run the server
// server.on('connection', (socket) => {
//     console.log('Connection Done');
// });
//location of the server = port
// server.listen(3000);
//open browser and type "hostport:3000" to communicate with the server

//2- Create JS Client
// var http = require("http");
// var server = http.createServer(function (req, res) {
//   //header data with HTTP code
//   res.writeHead(200, { "Content-type": "text/html" });
//   //check if the request = localhotst => response
//   if (req.url === "/") {
//     //data
//     res.write(
//       "<html><body><h1> Front End Server (Aramco Protal) </h1></body></html>"
//     );
//     //end the response
//     res.end();
//   } else if (req.url === "/api/products") {
//     //header data with HTTP code
//     res.writeHead(200, { "Content-type": "application/Json/" });
//     res.write(JSON.stringify({ name: "Kawthar", age: "25", Salary: "50K" }));
//     res.end();
//   } else if (req.url === "/admin")
//     //header data with HTTP code
//     res.writeHead(200, { "Content-type": "text/html" });
//   //check if the request = localhotst => response
//   if (req.url === "/") {
//     //data
//     res.write("<html><body><h1> Admin Panel </h1></body></html>");
//     //end the response
//     res.end();
//   } else {
//     res.end("Invalid Request Please try again with correct URL");
//   }
// });

// server.listen(4000);
// console.log("Node JS Web Server Taking Request in Port 4000");
var moment = require('moment');
moment().format;

